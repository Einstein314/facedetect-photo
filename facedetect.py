#C:\Users\Einstein\AppData\Local\Programs\Python\Python39\Lib\site-packages\cv2\data 模型位置
import cv2

#臉部模型
#cascade_path = "./models/haarcascade_frontalface_default.xml" 
# with open(cascade_path, mode="r", encoding="utf-8") as file:  #確認有讀取
#     resp = file.read()
# print(resp)
cascade_path = "./models/haarcascade_frontalface_alt.xml" 
#cascade_path = "./models/haarcascade_frontalface_alt2.xml" 
#cascade_path = "./models/haarcascade_frontalface_alt_three.xml" 

def run(imgPath=""): 
    outputPath = "./babyoutput.jpg"

    #圖片文件讀取
    image = cv2.imread(imgPath)
    #灰度化 分析照片前需要
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #分類器取得 cascade 中文: 瀑布、串聯
    cascade = cv2.CascadeClassifier(cascade_path)
    #臉型檢測
    facerect = cascade.detectMultiScale(image_gray, scaleFactor=1.1, minNeighbors=2, minSize=(30,30))
    #一個數組

    #找到臉了
    if len(facerect) > 0:
        # 畫出臉的位置 (長方形)
        for rect in facerect:
            cv2.rectangle(image, tuple(rect[0:2]), tuple(rect[0:2]+rect[2:4]), (0,255,0), thickness=3)
        #保存結果
        cv2.imwrite(outputPath, image)
        print("已偵測到臉部!")
    else:
        print("未偵測到臉部!")  
run(imgPath="./baby.jpg")              


